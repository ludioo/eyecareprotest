<?php

namespace App\Services;

use App\Contracts\ReportInterface;
use App\Meta;
use App\Services\Report\Criteria;

class ReportService implements ReportInterface
{
    /**
     *
     *
     *
     * @return mixed
     * @throws \Exception
     */
    public function generate()
    {
        $rules = $this->getDummyData();
        $model = $this->getModel('website');
        $fields = $this->getSelectFields('website');
        $groupWhereClause = 'OR';
        $argWhere = 'where';

        $iteration = 0;
        foreach ($rules as $ruleGroup) {
            if ($groupWhereClause == 'OR') {
                $argWhere = $iteration == 0 ? 'where' : 'orWhere';
            }
            $model = $model->$argWhere(
                $this->getWhereCallback($ruleGroup)
            );

            $iteration++;
        }

        return $model->select($fields)->get();
    }

    /**
     * @return array
     */
    protected function getDummyData()
    {
        $rules = [];

        $rules[] = [
            new Criteria('created_at', '2019-01-01', $this->getComparisonType('greater_or_equals')),
            new Criteria('domain', '.net', $this->getComparisonType('ends_with')),
        ];

        $rules[] = [
            new Criteria('domain', '.com', $this->getComparisonType('ends_with'))
        ];

        return $rules;
    }

    /**
     * Transforms a field request form
     *
     * @param $type
     *
     * @return mixed|string
     */
    public function getComparisonType($type)
    {
        $types = [
            'equals'            => '=',
            'not_equals'        => '!=',
            'greater_than'      => '>',
            'less_than'         => '<',
            'greater_or_equals' => '>=',
            'less_or_equals'    => '<=',
            'ends_with'         => 'LIKE',
        ];
        return isset($types[$type]) ? $types[$type] : '=';
    }

    protected function getModel($name)
    {
        $modelName = ucfirst($name);
        $modelClass = "\App\\" . $modelName;

        // Instantiate class.
        if (!class_exists($modelClass))
            throw new \Exception('Invalid classname');

        return new $modelClass();
    }

    /**
     * Get Select fields must return Available Fields for Report Visualization
     *
     * @param $model
     *
     * @return mixed
     */
    public function getSelectFields($model)
    {
        return Meta::whereModel($model)->pluck('name')->toArray();
    }

    /**
     * @param $criterias
     *
     * @return \Closure|null
     */
    public function getWhereCallback($criterias)
    {
        if (!count($criterias))
            return null;

        $callback = function ($q) use ($criterias) {
            foreach ($criterias as $criteria) {
                $q->where($criteria->getField(), $criteria->getComparison(), $criteria->getComparisonValue());
            }
        };
        return $callback;
    }
}
