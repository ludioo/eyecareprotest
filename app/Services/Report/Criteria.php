<?php
namespace App\Services\Report;

class Criteria
{
    /**
     * @var null
     */
    protected $field = null;

    /**
     * @var string
     */
    protected $comparison = '=';

    /**
     * @var null
     */
    protected $comparisonValue = null;


    public function __construct($field, $comparisonValue, $comparison = '=')
    {
        $this->field = $field;
        $this->comparison = $comparison;
        $this->comparisonValue = $comparisonValue;
    }

    /**
     * @param null $field
     */
    public function setField($field): void
    {
        $this->field = $field;
    }

    /**
     * @param null $comparisonValue
     */
    public function setComparisonValue($comparisonValue): void
    {
        $this->comparisonValue = $comparisonValue;
    }

    /**
     * @param string $comparison
     */
    public function setComparison(string $comparison): void
    {
        $this->comparison = $comparison;
    }

    /**
     * @return null
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @return null
     */
    public function getComparisonValue()
    {
        if ($this->getComparison() === 'LIKE') {
            return '%' . $this->comparisonValue;
        }

        return $this->comparisonValue;
    }

    /**
     * @return string
     */
    public function getComparison(): string
    {
        return $this->comparison;
    }
}
