<?php
namespace App\Contracts;


interface ReportInterface
{
    function getWhereCallback($criterias);
    function generate();
}
