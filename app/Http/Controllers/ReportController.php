<?php

namespace App\Http\Controllers;


use App\Services\ReportService;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    protected $service;

    public function __construct(ReportService $reportService)
    {
        $this->service = $reportService;
    }

    public function form()
    {
        return view('reports.form');
    }

    public function reportView(Request $request)
    {
        return response()->json($this->service->generate());
    }

}
