<?php

use Illuminate\Database\Seeder;

class MetaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $meta = [];
        $meta[] = [
            'name'  => 'domain',
            'label' => 'Domain',
            'type'  => 'text',
        ];
        $meta[] = [
            'name'  => 'name',
            'label' => 'Website Name',
            'type'  => 'text'
        ];

        foreach ($meta as $item) {
            \App\Meta::create([
                                  'model' => 'website',
                                  'name'  => $item['name'],
                                  'label' => $item['label'],
                                  'type'  => $item['type']
                              ]);
        }

    }
}
