<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;

class WebsiteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach(range(1, 10) as $index) {
            \DB::table('users')->insert(
                [
                    'name' => $faker->name,
                    'email' => $faker->email,
                    'password' => bcrypt('12345')
                ]
            );

            $domain = $faker->domainName;
            $siteName = explode(".", $domain);

            \DB::table('website')->insert(
                [
                    'name' => ucfirst($siteName[0]),
                    'domain' => $domain,
                    'user_id' => $index,
                    'created_at' => \Carbon\Carbon::createFromFormat('Y-m-d', $faker->date)
                ]
            );
        }
    }
}
