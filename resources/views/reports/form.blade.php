@extends('layouts.app')
@section('content')
	<div class="jumbotron">
		<h1>Report UX example</h1>
	</div>
	<div class="form">
		{!! Form::open() !!}
		{!! Form::select('model', 'Model', ['website' => 'Website', 'user' => 'User']) !!}

		<div class="row" id="clonable_row">
			<div class="col-md-4">
				{!! Form::select("criteria[0][0][field]", 'Field', ['created_at' => 'Created At', 'updated_at' => 'Updated At', 'name' => 'Name', 'domain' => 'Domain'])->attrs(['class' => 'criteria-field']) !!}
			</div>
			<div class="col-md-4">
				{!! Form::select("criteria[0][0][comparison_type]", 'Comparison', ['ends_with' => 'Ends With', 'equals' => 'Equals', 'greater_than' => 'Greater Than', 'less_than' => 'Less Than', 'greater_or_equals' => 'Greater Or Equals', 'less_or_equals' => 'Less Or Equals', 'not_equals' => 'Not Equals']) !!}
			</div>
			<div class="col-md-4">
				{!! Form::text('criteria[0][0][comparison_value]', 'Value') !!}
			</div>
		</div>

		<div class="criteria-extras">

		</div>

		<div class="row mb-5">
			<div class="col-md-12">
				<button type="button" class="btn btn-outline-primary" id="add_more_criteria">
					+ Add More
				</button>
			</div>
		</div>


		{!! Form::submit('Submit') !!}

		{!! Form::close() !!}
	</div>
@endsection
@push('scripts')
	<script>
        var criteriaCount = 0;

        $('#add_more_criteria').on('click', function (e) {
            e.preventDefault();
            criteriaCount++;
            $('#clonable_row').clone().attr('id', 'criteria-' + criteriaCount).appendTo('.criteria-extras');
        });

        $(document).ready(function() {
           $('.criteria-field').on('change', function(e) {
				console.log(e);
		   })
		});
	</script>
@endpush
