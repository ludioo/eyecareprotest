## EyeCarePro Test

#### Requirements
- Docker;
- Linux;

#### Steps to Reproduce.
- Pull repository, and install with ```docker-compose```.
- Go to docker container and open bash with command 
```docker exec -it eyecareproget-app bash```
- Copy .env.example for .env
- Generate an env key with ```php artisan key:generate```
- Run migrations with seeders: ```php artisan migrate --seed```
- Open browser and go to: //localhost/reports/1

#### About implementation
I've did a service which receives a group of criteria. A set of criteria group have a clause where decides if call method "WHERE" or "OR WHERE".

Also I did a "fake form" to generate data. 

It's all 

Regards

